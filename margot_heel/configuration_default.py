import os

def get_margot_repo_url():
    """
    Returns the html address of the margot repository
    """
    return 'https://gitlab.com/margot_project/core.git'

def get_margot_folder_name():
    """
    Returns the name of the folder that contains the mARGOt repository
    """
    return 'autotuner'

def get_env_var_name():
    """
    Returns the path of the environmental variable which hold the mARGOt
    root path for the active instance
    """
    return 'MARGOT_ROOT_PATH'

def get_build_dir_name():
    """
    Returns the name of the building directory inside the mARGOt repository
    """
    return 'build'

def get_margot_library_name():
    """
    Returns the name of the mARGOt libraraty inside the mARGOt repository
    """
    return os.path.join(get_build_dir_name(),'framework','libmargot.a')


def get_install_dir_name():
    """
    Returns the name of the install directory inside the mARGOt repository
    """
    return 'install'


def get_heel_dir_name():
    """
    Return the name of the heel interface inside the mARGOt repository
    """
    return os.path.join('margot_heel','margot_heel_if')


def get_heel_config_dir_name():
    """
    Returns the name of the configuration directory inside the mARGOt repository
    """
    return os.path.join(get_heel_dir_name(),'config')

def get_heel_build_dir_name():
    """
    Returns the name of the configuration directory inside the mARGOt repository
    """
    return os.path.join(get_heel_dir_name(),'build')

def get_heel_install_dir_name():
    """
    Returns the name of the configuration directory inside the mARGOt repository
    """
    return get_install_dir_name()

def get_heel_library_name():
    """
    Returns the name of the mARGOt heel library inside the mARGOt repository
    """
    return os.path.join(get_heel_build_dir_name(),'libmargot_heel.a')
