import os
import subprocess

from .error_handler import handle_fatal_error
from .configuration_default import get_margot_folder_name,get_env_var_name



def test_path_for_margot( target_path ):
    """
    It test whether the provided path contains the source code of mARGOt.
    In particular, it tries to open the CMake main file and parse the
    project name.
    """
    with open(os.path.join(target_path, 'CMakeLists.txt'), 'r') as infile:
        for line in infile.readlines():
            if 'project( mARGOt )' in line:
                return target_path
    raise ValueError('The path does not contain the mARGOt source code')


def find_margot_from_hint( hint ):
    """
    This function aims at finding margot starting from a hint. In particular, it
    will tries to test the hint path and its parent directories until it reaches
    the fs root. It will also try to append the name of the autotuner framework.
    """

    # get the absolute real path
    real_path = os.path.realpath(hint)

    # assume that the user has provided the correct path
    try:
        margot_path = test_path_for_margot(real_path)
    except:

        # try to append the autotuner folder
        try:
            margot_path = test_path_for_margot(os.path.join(real_path, get_margot_folder_name()))
        except:

            # split the path and look for alternatives path
            eligible_paths = []
            path_head = os.path.dirname(real_path)
            fs_root = os.path.abspath(os.sep)
            while path_head != fs_root:
                eligible_paths.append(path_head)
                eligible_paths.append(os.path.join(path_head, get_margot_folder_name()))
                path_head = os.path.dirname(path_head)
            eligible_paths.append(fs_root)
            eligible_paths.append(os.path.join(fs_root, get_margot_folder_name()))

            # try to find the first correct path
            for eligible_path in eligible_paths:
                try:
                    margot_path = test_path_for_margot(eligible_path)
                    break
                except:
                    pass

    # return the found path (if any)
    return margot_path


def get_margot_root_path( hint = '' ):
    """
    This function aims at finding a mARGOt instance to apply commands, starting from
    a hint path. It uses the following order:
        1) It uses the hint parameter if it has been set, otherwise
        2) It looks for an environmental variable named MARGOT_ROOT_PATH, otherwise
        3) It will use the current working directory
    """

    if hint:
        try:
            return find_margot_from_hint(hint)
        except UnboundLocalError as err:
            return ''

    if get_env_var_name() in os.environ:
        try:
            return find_margot_from_hint(os.environ[get_env_var_name()])
        except UnboundLocalError as err:
            return ''

    try:
        return find_margot_from_hint('.')
    except UnboundLocalError as err:
        return ''
