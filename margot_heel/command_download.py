import subprocess
import os

from .error_handler import handle_fatal_error
from .configuration_default import get_margot_repo_url,get_margot_folder_name
from .dependencies_utils import check_command_version


def download_margot( where, branch ):
    """
    This function just download margot quitely
    """
    command = ['git', 'clone', '--branch', branch, get_margot_repo_url(), where]
    try:
        return_code = subprocess.call(command)
        if return_code != os.EX_OK:
            raise ValueError('Return code: {0}'.format(return_code))
    except ValueError as err:
        handle_fatal_error('Unable to clone the mARGOt repository',err)


def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # this operation requires git, make sure to have it
    print('Check for requirements...')
    check_command_version('git')
    print()

    # clone the repository
    cloning_path = os.path.realpath(os.path.join(args.where,get_margot_folder_name()))
    download_margot(cloning_path, args.branch)
