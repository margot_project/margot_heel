import os

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .cmake_utils import set_build_option


def execute_command( args, enable ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the name of information to retrieve
    topic_name = args.buildopt

    # get the path to the margot root
    margot_root = get_margot_root_path()

    # react to the search
    if not margot_root:
        handle_fatal_error('Unable to find any mARGOt sources in the current working directory',ValueError("Unable to find mARGOt"))

    # print the build options directly from the cmake cache file
    if enable:
        print('Enabling the build option "{0}"'.format(topic_name))
        set_build_option(margot_root, topic_name, True)
    else:
        print('Disabling the build option "{0}"'.format(topic_name))
        set_build_option(margot_root, topic_name, False)
