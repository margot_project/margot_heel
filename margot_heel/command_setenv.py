import os
import subprocess

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .configuration_default import get_env_var_name



def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the path from the argument
    margot_path = get_margot_root_path(args.where)

    # react to the search
    if margot_path:
        with open(args.file_name, 'w') as outfile:
            outfile.write('export {0}={1}\n'.format(get_env_var_name(),margot_path))
        print('Found mARGOt instance in "{0}"'.format(margot_path))
        print('Please, source environment script "{0}"'.format(os.path.realpath(args.file_name)))
    else:
        handle_fatal_error('Unable to find any mARGOt sources in "{0}"'.format(args.where),ValueError("Unable to use the provided path"))
