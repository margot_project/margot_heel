import os
import shutil
import filecmp
import subprocess

from .error_handler import handle_fatal_error
from .configuration_default import get_heel_build_dir_name,get_heel_install_dir_name,get_heel_config_dir_name,get_heel_dir_name,get_heel_library_name
from .cmake_utils import run_cmake


def replace_files( configuration_folder, old_files, new_files ):
    """
    This function deletes all the old files and replace them with the new ones
    """

    # at first remove the old files
    for old_file in old_files:
        os.unlink(old_file)

    # then copy the new one in the right place (make sure that it ends with .conf)
    for new_file in new_files:
        file_name = os.path.basename(new_file)
        if not file_name.endswith('.conf'):
            file_name = '{0}.conf'.format(file_name)
        new_file_path = os.path.join(configuration_folder, file_name)
        shutil.copyfile(new_file, new_file_path, follow_symlinks=True)



def prepare_heel_folder( margot_root, margot_configuration, ops = []):
    """
    This functions prepares the high level interface for the given configuration file,
    while tring to avoid to recompile the interface each time

    Input parameters:
        - margot_configuration -> string, the path to the margot configuration file in XML
        - ops -> list, the list of Operating Points file in XML

    Return value:
        boolean, True if we need to recompile the heel folder
    """

    # check if we already have a heel library
    to_build_heel = not os.path.isfile(os.path.join(margot_root, get_heel_library_name()))

    # get all the configuration files (the one that ends with .conf)
    configuration_folder = os.path.join(margot_root, get_heel_config_dir_name())
    for root, dirs, files in os.walk(configuration_folder):
        configuration_files = [os.path.join(root,x) for x in files if x.endswith('.conf')]
        break

    # build the list of the new configuration files
    new_configuration_files = [ margot_configuration ]
    new_configuration_files.extend(ops)

    # compare them by size
    if len(new_configuration_files) != len(configuration_files):
        replace_files(configuration_folder, configuration_files, new_configuration_files)
        return True


    # look if the margot configuration file is renamed
    new_margot_configuration_file = margot_configuration if margot_configuration.endswith('.conf') else '{0}.conf'.format(margot_configuration)
    possible_old_configuration_files = [ os.path.basename(x) for x in configuration_files]
    if not new_margot_configuration_file in possible_old_configuration_files:
        replace_files(configuration_folder, configuration_files, new_configuration_files)
        return True




    # compare them by content
    for new_file in new_configuration_files:
        found_a_similar_one = False
        for old_file in configuration_files:
            if filecmp.cmp(old_file, new_file, shallow=False):
                found_a_similar_one = True
                break
        if not found_a_similar_one:
            replace_files(configuration_folder, configuration_files, new_configuration_files)
            return True

    # they are the same
    return to_build_heel



def build_high_level_interface( margot_root, margot_configuration_file_name ):
    """
    This function generates and build the high level interface

    Input parameters:
        - margot_root -> string, path to the margot root path
        - margot_configuration_file_name -> string, the basename of the margot_configuration_file
    """

    # compute the build and install path
    build_path = os.path.join(margot_root, get_heel_build_dir_name())
    install_path = os.path.join(margot_root, get_heel_install_dir_name())
    source_path = os.path.join(margot_root, get_heel_dir_name())

    # run cmake
    print('Bootstraping the mARGOt heel interface...')
    run_cmake(margot_root, options = ['-DMARGOT_CONF_FILE={0}'.format(margot_configuration_file_name)], install_path = install_path, build_path = build_path, source_path = source_path )
    print()

    # build and install the library
    print('Building the mARGOt heel interface...')
    try:
        return_code = subprocess.call(['make', '-C', build_path, 'install'])
        if return_code != os.EX_OK:
            handle_fatal_error('Unable to compile mARGOt heel', ValueError('Return code of make was {0}'.format(return_code)))
    except subprocess.CalledProcessError as err:
        handle_fatal_error('Unable to compile mARGOt heel', err)
    print()
