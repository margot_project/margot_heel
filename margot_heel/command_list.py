import os

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .cmake_utils import get_cached_value,get_avail_build_opts


def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the name of information to retrieve
    topic_name = args.what

    # get the path to the margot root
    margot_root = get_margot_root_path()

    # react to the search
    if not margot_root:
        handle_fatal_error('Unable to find any mARGOt sources in the current working directory',ValueError("Unable to find mARGOt"))

    # print the build options directly from the cmake cache file
    if args.what == 'buildopts':

        # get all the available options
        available_options = [x[0].lower() for x in get_avail_build_opts(margot_root)]

        # loop over them to retrieve their value
        options = []
        for opt_name in available_options:
            options.append(get_cached_value(margot_root, opt_name))


        # add padding to align them
        max_name_lenght = max([ len(x[0]) for x in options])

        # print the message
        print('The mARGOt autotuner build options are configured as follows:')
        if options:
            for opt in options:
                print('\t{0:{2}s} [{1}]'.format(opt[0].lower(), opt[1].lower(), max_name_lenght))
        else:
            print('\tNo building option is available')
