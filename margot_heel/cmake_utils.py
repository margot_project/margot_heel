import os
import subprocess
import errno

from .error_handler import handle_fatal_error
from .configuration_default import get_build_dir_name,get_install_dir_name


def mkdir_p(p):
  """
  Helper function that emulate the command "mkdir -p"
    p -> the path to be created
  """
  try:
    os.makedirs(p)
  except OSError as exc: # Python >2.5
    if exc.errno == errno.EEXIST and os.path.isdir(p):
      pass
    else:
      handle_fatal_error('Unable to create the directory "{0}"'.format(p),exc)



def parse_buildopts_from_cmake( text ):
    """
    This function extracts the option name, the option description and the
    default option value in pretty format

    It returns a list wit [name,description,default_value]
    """
    # we need to parse something in the format of
    # option( WITH_TEST "Build a test unit application" OFF )

    # the first option is to get the part inside the parenthesis
    inner_part = text.lower().split('(')[1].split(')')[0]

    # then we need to tokenize with quote characters and strip it
    return [ x.strip() for x in inner_part.split('"') if x ]


def get_avail_build_opts( margot_root_path ):
    """
    This function open the cmake file of the mARGOt source files to seee the
    building options available
    """

    # get all the options
    options = []
    with open(os.path.join(margot_root_path, 'CMakeLists.txt'), 'r') as infile:
        for line in infile.readlines():
            if line.strip().lower().startswith('option'):
                options.append(parse_buildopts_from_cmake(line))
    return options



def get_cmake_required_version( margot_root_path ):
    """
    This function open the cmake file of the mARGOt source files to seee if the
    project requires a minimum version of cmake
    """
    with open(os.path.join(margot_root_path, 'CMakeLists.txt'), 'r') as infile:
        for line in infile.readlines():
            if line.strip().lower().startswith('cmake_minimum_required'):
                inner_part = line.split('(')[1].split(')')[0].strip()
                tokens = [x.lower() for x in inner_part.split(' ') if x]
                for index_token, token in enumerate(tokens):
                    if token == 'version':
                        return tokens[index_token+1]


def run_cmake( margot_root, options = [], install_path = '', build_path = '', source_path = '' ):
    """
    This function runs cmake quitely

    Input parameters:
        - where -> string, the root path of the margot repository
    """
    if not build_path:
        build_path = os.path.join(margot_root,get_build_dir_name())
    if not install_path:
        install_path = os.path.join(margot_root,get_install_dir_name())
    if not source_path:
        source_path = margot_root
    mkdir_p(build_path)
    command = ['cmake', os.path.realpath(source_path), '-DCMAKE_INSTALL_PREFIX:PATH={0}'.format(install_path)]
    if options:
        command.extend(options)
    try:
        cmake_instance = subprocess.Popen(command, cwd=build_path)
        return_code = cmake_instance.wait()
        if return_code != os.EX_OK:
            raise ValueError('Return code: {0}'.format(return_code))
    except ValueError as err:
        handle_fatal_error('Unable to run cmake correctly',err)



def get_cached_value( margot_root, option_name, no_except = False ):
    """
    This function extract from the cmake cache the value currently set for that option

    Input parameters:
        - margot_root -> string, the root path of the margot repository
        - option_name -> string, the name of the target build options

    Return value:
    list with the actual name and value of the program option:
        - [0] -> the actual name of the build option
        - [1] -> the actual value of the build option
    """
    cmake_cache_path = os.path.join(margot_root,get_build_dir_name(),'CMakeCache.txt')
    real_option_name = option_name.lower()
    option_value = ''

    try:
        with open(cmake_cache_path, 'r') as infile:
            for line in infile.readlines():
                post_processed_line = line.strip()
                if post_processed_line.lower().startswith(real_option_name):
                    actual_name,actual_value = post_processed_line.split('=')
                    return [actual_name.strip(), actual_value.strip()]
    except FileNotFoundError as err:
        if no_except:
            return ['','']
        else:
            handle_fatal_error('Unable read the CMake cache file (bootstrap might fix it)',err)

    handle_fatal_error('The build option "{0}" is not available'.format(option_name),ValueError('Build option not found'))


def set_build_option( margot_root, option_name, option_value ):
    """
    This function enables the target build option

    Input parameters:
        - margot_root -> string, the path to the mARGOt repository
        - option_name -> string, the name of the build option
        - option_value -> boolean, True if we want to enable the option
    """

    # get the previous option name and value
    cmake_cache_path = os.path.join(margot_root,get_build_dir_name(),'CMakeCache.txt')
    real_option_name = option_name.lower()
    option_cmake_name, option_cmake_value = get_cached_value(margot_root, real_option_name)

    # compose the flag to pass to margot
    flag_value = 'ON' if option_value else 'OFF'
    cmake_flag = '-D{0}={1}'.format(option_cmake_name.upper(), flag_value)

    # run cmake
    run_cmake(margot_root, [cmake_flag])
