# The mARGOt heel command line interface

This repository contains an utility tool in python to configure the mARGOt autotuning framework from a command line interface.
The goal of the tool is to ease the integration process of the autotuner in the target application.


## How to use the tool

In the current version, the tool exports commands that guide the download, configuration end installation of mARGOt automatically. The main idea is to clone and manage the autotuner in a dedicated folder named "autotuner".

This tool will install all the libraries and headers in a folder named "autotuner/install" using the classical build conventions. With this approach, each application uses an autotuner tailored for its requirements. To further facilitate the integration process, this tools exposes the following build configuration files that application developers might use to integrate mARGOt:

> autotuner/install/lib/cmake/FindMARGOT_HEEL.cmake

> autotuner/install/lib/pkgconfig/margot_heel.pc

To use the tool, clone the repository and source an environmental file that creates an alias for the tool:

```
git clone https://gitlab.com/margot_project/margot_heel.git
cd margot_heel
source enable_margot.env
margot help
```


## Exposed commands

The utility tools comes with an help function that provides much more details about the syntax and customization of each command.

The following are the main commands exposed by the tool:

#### margot download

It clones the mARGOt autotuner in a dedicated folder

#### margot build MARGOT\_CONF\_FILE \[ OP\_FILE\ \[ OP\_FILE\] ... \]

It configures mARGOt according to the main MARGOT\_CONF\_FILE and the optional Operating Points files that describes the application knowledge.
For details about the semantics of the configuration files refer to the [core repository](https://gitlab.com/margot_project/core.git).


## Contribution guidelines

Clone the repository and send pull requests, any contribution is welcome.


### Who do I talk to?
Contact: davide [dot] gadioli [at] polimi [dot] it
Organization: Politecnico di Milano, Italy