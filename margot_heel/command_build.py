import os
import subprocess

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .dependencies_utils import check_command_version,is_agora_requested_in_config,is_monitor_requested_in_config
from .cmake_utils import get_cached_value,set_build_option,run_cmake
from .configuration_default import get_build_dir_name,get_margot_library_name
from .heel_utils import prepare_heel_folder,build_high_level_interface





def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the path to the margot root
    suggested_path = os.path.realpath('.')
    margot_root = get_margot_root_path(suggested_path)

    # check if we already have a margot library built in this autotuner
    is_margot_lib_available = os.path.isfile(os.path.join(margot_root,get_margot_library_name()))

    # check if the configuration files requires some additional components of the framework
    try:
        is_with_agora = is_agora_requested_in_config(args.margot_file)
        is_with_temperature_monitor = is_monitor_requested_in_config(args.margot_file, 'temperature')
        is_with_papi_monitor = is_monitor_requested_in_config(args.margot_file, 'papi')
        is_with_collector_monitor = is_monitor_requested_in_config(args.margot_file, 'collector')
    except FileNotFoundError as err:
        handle_fatal_error('Unable to read the mARGOt configuration file', err)

    # check if we need to perform additional configuration because of user laziness
    to_bootstrap = get_cached_value(margot_root, 'with_agora', no_except=True)[0] == '' # if there is a chache list
    enable_agora = is_with_agora
    enable_temperature_monitor = is_with_temperature_monitor
    enable_papi_monitor = is_with_papi_monitor
    enable_collector_monitor = is_with_collector_monitor
    if margot_root:

        # check for agora
        agora_value = get_cached_value(margot_root, 'with_agora', no_except=True)[1].upper()
        if agora_value == 'ON' and is_with_agora:
            enable_agora = False

        # check for temperature monitor
        temperature_monitor_value = get_cached_value(margot_root, 'use_temperature_monitor', no_except=True)[1].upper()
        if temperature_monitor_value == 'ON' and is_with_temperature_monitor:
            enable_temperature_monitor = False

        # check for collector monitor
        collector_monitor_value = get_cached_value(margot_root, 'use_collector_monitor', no_except=True)[1].upper()
        if collector_monitor_value == 'ON' and is_with_collector_monitor:
            enable_collector_monitor = False

        # check for papi monitor
        papi_monitor_value = get_cached_value(margot_root, 'use_papi_monitor', no_except=True)[1].upper()
        if papi_monitor_value == 'ON' and is_with_papi_monitor:
            enable_papi_monitor = False

        # check if we need to bootstrap it (using the agora flag)
        if agora_value:
            to_bootstrap = False

    # ok, check if we need to compile margot from configuration files
    compile_margot = not is_margot_lib_available
    we_need_cmake = False
    if to_bootstrap or enable_agora or enable_temperature_monitor or enable_papi_monitor or enable_collector_monitor:
        compile_margot = True
        we_need_cmake = True


    # this operation requires tools, make sure to have it before doing stuff
    if (not margot_root) or we_need_cmake:
        print('Check for requirements...')
        if not margot_root:
            check_command_version('git')
        if we_need_cmake:
            check_command_version('cmake',margot_path=margot_root)
        print()

    # if no margot is available download it now and configure it
    if not margot_root:
        from .command_download import download_margot
        download_margot('.','master')
        margot_root = get_margot_root_path('.')
        print()

    # bootstrap the framework
    if to_bootstrap:
        print('Initialize the mARGOt build system...')
        run_cmake(margot_root)
        print()

    # check if we need to enable agora
    if enable_agora:
        print('Configuring mARGOt to enable agora...')
        set_build_option(margot_root, 'with_agora', True)
        print()

    # check if we need to enable optional monitors
    if enable_temperature_monitor:
        print('Configuring mARGOt to enable temperature monitor...')
        set_build_option(margot_root, 'use_temperature_monitor', True)
        print()
    if enable_collector_monitor:
        print('Configuring mARGOt to enable collector monitor...')
        set_build_option(margot_root, 'use_collector_monitor', True)
        print()
    if enable_papi_monitor:
        print('Configuring mARGOt to enable papi monitor...')
        set_build_option(margot_root, 'use_papi_monitor', True)
        print()


    # compile the mARGOt autotuner (if needed)
    if compile_margot:
        print('Building mARGOt...')
        try:
            return_code = subprocess.call(['make', '-C', os.path.join(margot_root, get_build_dir_name()), 'install'])
            if return_code != os.EX_OK:
                handle_fatal_error('Unable to compile mARGOt', ValueError('Return code of make was {0}'.format(return_code)))
        except CalledProcessError as err:
            handle_fatal_error('Unable to compile mARGOt', err)
        print()

    # prepare the high level interface for the application (make sure the margot file ends with .conf)
    build_heel = prepare_heel_folder(margot_root, args.margot_file, args.ops)


    # now we need to compile it
    if build_heel:
        margot_configuration_file = os.path.basename(args.margot_file if args.margot_file.endswith('.conf') else '{0}.conf'.format(args.margot_file))
        build_high_level_interface(margot_root,margot_configuration_file)

    # print an info message if nothing to do
    if (not build_heel) and (not compile_margot):
        print('The previous build is up to date, nothing to do...')
