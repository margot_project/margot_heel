#!/usr/bin/env python3

# minimal imports to load the actual margot library and to handle input arguments
import os
import inspect
import sys
import argparse


# load the margot command line library assuming that it is in the
# system path, for example due to an installation script
try:
    import margot_heel
except ImportError:

    # try to guess the position of the library inside the repository
    src_folder = os.path.realpath(os.path.split(inspect.getfile( inspect.currentframe() ))[0])
    if src_folder not in sys.path:
        sys.path.append(src_folder)

    # perform another attempt to load the library
    try:
        import margot_heel
    except ImportError as err:
        print('[ERROR]: Unable to import the margot_heel library!')
        raise err


if __name__ == '__main__':

    # create the main argument parser for margot
    arg_parser = argparse.ArgumentParser(description='The mARGOt command line interface, a swiss army knife for managing the autotuner framework integration')
    arg_parser.add_argument('--version', action='version', version='mARGOt cli v0.1', help='print the version of the tools and exit')
    subparsers = arg_parser.add_subparsers(dest='command', help='the commands exposed by this tool')

    # add the command to print the hel message (even if redundant)
    command_parser = subparsers.add_parser('help', help='print the help message and exit')

    # add the command to download the autotuner source
    command_parser = subparsers.add_parser('download', help='download the mARGOt autotuning framework')
    command_parser.add_argument('where', metavar='PATH', type=str, nargs='?', default='.',
        help='the path for cloning the repository')
    command_parser.add_argument('--branch', metavar='BRANCH', type=str, nargs='?', default='master', dest='branch',
        help='the name of the target branch of the repository')


    # add the command to switch between autotuners
    command_parser = subparsers.add_parser('setenv', help='this command generates the environmental script for the target autotuner')
    command_parser.add_argument('where', metavar='PATH', type=str, nargs='?', default='.',
        help='the root path of the target mARGOt repository')
    command_parser.add_argument('file_name', metavar='FILE_NAME', type=str, nargs='?', default='margot.env',
        help='the name of the environmental file')

    # add the command to list available stuff
    list_available_commands = ['buildopts']
    command_parser = subparsers.add_parser('avail', help='this command prints available information about mARGOt')
    command_parser.add_argument('what', metavar='WHAT', choices=list_available_commands,
        help='the target information to retrieve ["{0}"]'.format('", "'.join(list_available_commands)))

    # add the command to list current build options
    command_parser = subparsers.add_parser('list', help='this command prints information about the current mARGOt setup')
    command_parser.add_argument('what', metavar='WHAT', choices=list_available_commands,
        help='the target information to retrieve ["{0}"]'.format('", "'.join(list_available_commands)))

    # add the command to enable a building option
    command_parser = subparsers.add_parser('enable', help='this command enable a building option of mARGOt')
    command_parser.add_argument('buildopt', metavar='OPTION',
        help='the name of the target building option to enable')

    # add the command to disable a building option
    command_parser = subparsers.add_parser('disable', help='this command disable a building option of mARGOt')
    command_parser.add_argument('buildopt', metavar='OPTION',
        help='the name of the target building option to disable')

    # add the command to run the cmake on the project
    command_parser = subparsers.add_parser('bootstrap', help='this command bootstrap and configure the mARGOt build system (e.g. check for dependencies)')
    command_parser.add_argument('--where', metavar='WHERE', nargs='?', default='', dest='where',
        help='the path of the target mARGOt sources (if different by default)')

    # add the command to configure and build the framework with the high level interface
    command_parser = subparsers.add_parser('build', help='this command generates the high level interface and build the framework')
    command_parser.add_argument('margot_file', metavar='MARGOT_CONFIG_FILE',
        help='the path of the mARGOt XML configuration file')
    command_parser.add_argument('ops', metavar='OPERATING_POINTS_FILE', nargs='*',
        help='the path of the Operating Points XML description files (if any)')

    # actually parse the commands
    args = arg_parser.parse_args()


    # ---- handle the different commands

    if not args.command:
        arg_parser.print_help()

    if args.command == 'help':
        arg_parser.print_help()

    if args.command == 'download':
        from margot_heel import command_download as commander
        commander.execute_command(args)

    if args.command == 'setenv':
        from margot_heel import command_setenv as commander
        commander.execute_command(args)

    if args.command == 'avail':
        from margot_heel import command_avail as commander
        commander.execute_command(args)

    if args.command == 'list':
        from margot_heel import command_list as commander
        commander.execute_command(args)

    if args.command == 'enable':
        from margot_heel import command_enable_disable as commander
        commander.execute_command(args, True)

    if args.command == 'disable':
        from margot_heel import command_enable_disable as commander
        commander.execute_command(args, False)

    if args.command == 'bootstrap':
        from margot_heel import command_bootstrap as commander
        commander.execute_command(args)

    if args.command == 'build':
        from margot_heel import command_build as commander
        commander.execute_command(args)
