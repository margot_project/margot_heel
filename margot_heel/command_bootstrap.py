import os

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .dependencies_utils import check_command_version
from .cmake_utils import run_cmake





def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the path to the margot root
    suggested_path = os.path.realpath(args.where if args.where else '.')
    margot_root = get_margot_root_path(suggested_path)

    # this operation requires tools, make sure to have it before doing stuff
    print('Check for requirements...')
    if not margot_root:
        check_command_version('git')
    check_command_version('cmake',margot_path=margot_root)
    print()

    # if no margot is available download it now
    if not margot_root:
        from .command_download import download_margot
        download_margot('.','master')
        margot_root = get_margot_root_path('.')

    # run cmake
    print('Initialize the mARGOt build system...')
    run_cmake(margot_root)
