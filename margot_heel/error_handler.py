def handle_fatal_error( error_message, exception = None ):
    """
    This function pretty print fatal error messages for end user in
    a consistent way, then it raise again the exception for having
    stack trace.

    Input parameters:
        - error_message -> string vector of strings, each one represents
                           a line in the error message.
        - exception     -> the original exception that triggered the
                           fatal error, raised at the end of fucntion.
    """

    # print the error message header
    print()
    print('-------//////////////// FATAL ERROR \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\-------')

    # handle the correct types of input messages
    if isinstance(error_message, str):
        print(error_message)
    else:
        if isinstance(error_message, list):
            for error_line in error_message:
                print(error_line)
        else:
            # the input message is weird, try to print it anyhow
            print(error_message)


    # print the error message trailer
    print('-----------------------------------------------------------')
    print()

    # finally, rise again the exception to have a real stack trace
    if exception:
        raise exception
