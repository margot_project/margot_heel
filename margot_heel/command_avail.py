import os

from .error_handler import handle_fatal_error
from .find_margot_instance import get_margot_root_path
from .cmake_utils import get_avail_build_opts


def execute_command( args ):
    """
    This function is the entry point for executing the command handled
    by this python script

    Input parameter:
        - args  -> The parsed arguments
    """

    # get the name of information to retrieve
    topic_name = args.what

    # get the path to the margot root
    margot_root = get_margot_root_path()

    # react to the search
    if not margot_root:
        handle_fatal_error('Unable to find any mARGOt sources in the current working directory',ValueError("Unable to find mARGOt"))

    # print the build options directly from the cmake file
    if args.what == 'buildopts':

        # get all the options
        options = get_avail_build_opts(margot_root)

        # add padding to align them
        max_name_lenght = max([ len(x[0]) for x in options])
        max_desc_lenght = max([ len(x[1]) for x in options])

        # print the message
        print('Available building options and default values:')
        if options:
            for opt in options:
                print('\t{0:{3}s} : {1:{4}s} default = {2}'.format(opt[0].lower(), opt[1],opt[2],max_name_lenght,max_desc_lenght))
        else:
            print('\tNo building option is available')
