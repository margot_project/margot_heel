import subprocess
import os

from .cmake_utils import get_cmake_required_version
from .error_handler import handle_fatal_error

supported_commands = [
    'git','cmake'
]

version_flags = {
    'git':'--version',
    'cmake':'--version'
}

def check_command_version( command_name, quiet = False, margot_path = '' ):
    """
    This function tests whether the given command is available on the target
    machine, and which version it is, printed on the stdout

    Input parameters:
        - command_name -> string with the name of the command
        - required_version -> string with the minimum version string
        - quite -> boolean, if true it doesn't print anything on the stdout
    """

    # figure out the flag for retrieving the flag
    try:
        flag_name = version_flags[command_name]
    except KeyError:
        flag_name = '--version'

    # ask for a version
    try:
        command_output = subprocess.check_output([command_name, flag_name]).decode("utf-8")
    except CalledProcessError as err:
        handle_fatal_error("Unable to execute the \"{0}\" command".format(command_name), err)
    except UnicodeError as err:
        print('Command {0} available, but not able to understand the version'.format(command_name))
        return

    # now we strip the output and keep the first line
    command_version = command_output.split(os.linesep)[0]

    # if we are checking cmake, we might want to verify the version according to margot
    if command_name == 'cmake' and margot_path:
        requested_version_str = get_cmake_required_version(margot_path)
        requested_version = [int(x) for x in requested_version_str.split('.')]
        tokenized_version = [x.strip() for x in command_version.split() if x]
        for token in tokenized_version:
            try:
                version_tokens = [ int(x) for x in token.split('.') if x]
                command_is_admissible = True
                for index_version, version_value in enumerate(version_tokens):
                    if version_value < requested_version[index_version]:
                        command_is_admissible = False
                        break
                    elif version_value > requested_version[index_version]:
                        break
                if not command_is_admissible:
                    handle_fatal_error("The command \"{0}\" has version {1}, requested at least {2}".format(command_name, token, requested_version_str), IndexError('Command version mismatch'))

            except ValueError as err:
                pass

    # print the result
    if not quiet :
        if command_name == 'cmake' and margot_path:
            print('{0} [requested {1}]'.format(command_version,requested_version_str))
        else:
            print(command_version)


def is_agora_requested_in_config( margot_config_file):
    """
    This function reads the margot configuration file to see if it requires agora

    Input parameters:
        - margot_config_file -> string, the path to the margot configuration file

    Return value:
        True, if the configuration file requires agora
        False, otherwise
    """

    with open(margot_config_file, 'r') as infile:
         for line in infile.readlines():
             post_processed_line = line.strip()
             if post_processed_line.lower().startswith('<agora'):
                 return True
    return False


def is_monitor_requested_in_config( margot_config_file, monitor_type):
    """
    This function reads the margot configuration file to see if it requires
    the requested monitor.

    Input parameters:
        - margot_config_file -> string, the path to the margot configuration file
        - monitor_name -> string, the name of the target monitor

    Return value:
        True, if the configuration file requires the monitor
        False, otherwise
    """

    with open(margot_config_file, 'r') as infile:
         for line in infile.readlines():
             post_processed_line = line.strip().lower()
             if post_processed_line.startswith('<monitor') and ('type="{0}"'.format(monitor_type.lower()) in post_processed_line):
                 return True
    return False
